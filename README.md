# testdir

## Deprecation Notice
Since implementing this crate, I have found [assert_fs](https://crates.io/crates/assert_fs), which provides the same features and more. It's maintained by the Rust team, so unlike with other existing crates, I have no concerns pulling it into all my projects. Therefore I will use assert_fs going forward and not develop this crate further.

---

Tiny utility for Rust programmers to create a unique file system directory in automated tests and have it automatically deleted afterwards.

You can find plenty similar crates, which you might prefer. This crate is mostly intended for personal use, but feel free to use it, if it works for you.

To include it in your project, you can depend on it like so in your Cargo.toml:

```toml
[dev-dependencies]
testdir = { git = "https://codeberg.org/trem/testdir.git", tag = "v1.0.0" }
```
