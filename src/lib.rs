use std::fs;
use std::ops::Deref;
use std::path::PathBuf;


/// ```
/// use testdir::TestDir;
/// {
///     let testdir = TestDir::new();
///     //Use testdir like a PathBuf.
/// } //When the scope ends, testdir deletes itself from the file system.
/// ```
#[derive(Clone, PartialEq, Eq)]
pub struct TestDir {
    path: PathBuf,
}

impl TestDir {
    pub fn new() -> Self {
        let crate_name = std::env::var("CARGO_PKG_NAME").expect("The environment variable 'CARGO_PKG_NAME' could not be found!");

        let test_dir = std::env::temp_dir()
            .join(format!(
                "{}-test-{}",
                crate_name,
                std::thread::current().name().expect("Failed to get thread name for unit test!")
            ));

        fs::create_dir_all(&test_dir)
            .unwrap_or_else(|cause| panic!("Failed to create test directory '{}': {}", &test_dir.display(), cause));

        Self {
            path: test_dir
        }
    }
}

impl std::default::Default for TestDir {
    fn default() ->  Self {
        Self::new()
    }
}

impl std::ops::Deref for TestDir {
    type Target = PathBuf;
    fn deref(&self) -> &PathBuf {
        &self.path
    }
}
impl AsRef<std::path::Path> for TestDir {
    fn as_ref(&self) -> &std::path::Path {
        self.deref().as_ref()
    }
}

impl Drop for TestDir {
    fn drop(&mut self) {
        fs::remove_dir_all(&self.path)
            .unwrap_or_else(|cause| panic!("Failed to remove test directory '{}' after test: {}", &self.path.display(), cause));
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_and_destruct() {

        let testdir = TestDir::new();
        let testdir_path = testdir.path.clone();

        assert!(testdir.is_dir());

        let file_path = testdir.join("hello_world.txt");
        fs::write(&file_path, "Hello, World!").unwrap();

        drop(testdir);

        assert!(!testdir_path.exists());
    }
}
